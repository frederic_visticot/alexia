import 'package:flutter/material.dart';
import 'package:monamoureuse/step_port_indication_page.dart';

class StepCountLovePage extends StatefulWidget {
  @override
  _StepCountLovePageState createState() => _StepCountLovePageState();
}

class _StepCountLovePageState extends State<StepCountLovePage> {
  int nbAttempts = 0;
  final int resultOK = 52;

  _checkResult(int result) {
    if (result == resultOK) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => StepPortIndicationPage()));
    } else {
      nbAttempts++;
      _showError();
    }
  }

  Future<void> _showError() async {
    var lost = false;
    var message = "Concentre toi autrement c'est perdu ...";
    var nbAttemptsMessage = "Plus que ${2 - nbAttempts} essai !";
    if (2 - nbAttempts == 0) {
      nbAttemptsMessage = "Plus d'essai !";
    }
    if (nbAttempts == 2) {
      nbAttempts = 0;
      message = "Tu as de la chance, tu as vies illimitées";
      lost = true;
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Attention mauvaise réponse'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(nbAttemptsMessage),
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK, je vais me concentrer'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  "Combien y a t'il de coeurs dans la boite ? \n\r Attention, tu n'as que 2 essais",
                  style: TextStyle(fontSize: 24),
                  textAlign: TextAlign.center,
                ),
              ),
              Column(
                children: [
                  ButtonTheme(
                    minWidth: 200,
                    child: ElevatedButton(
                      child: Text("41"),
                      onPressed: () {
                        _checkResult(41);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    child: Text("48"),
                    onPressed: () {
                      _checkResult(48);
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    child: Text("52"),
                    onPressed: () {
                      _checkResult(52);
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    child: Text("62"),
                    onPressed: () {
                      _checkResult(62);
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              )
            ],
          )),
        ]),
      ),
    );
  }
}
