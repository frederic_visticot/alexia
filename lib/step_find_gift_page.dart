import 'package:flutter/material.dart';
import 'package:monamoureuse/step_conclusion_page.dart';

class StepFindGiftPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Text(
                    "Félicitation, plus qu'à trouver le trésor qui se trouve caché à cet endroit\n\r Cliques sur le bouton quand tu as trouvé le trésor",
                    style: TextStyle(fontSize: 24),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Image.asset("step3.webp"))
              ],
            ),
          ),
          ElevatedButton(
            child: Text("J'ai trouvé le trésor"),
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => StepConclusionPage()));
            },
          )
        ]),
      ),
    );
  }
}
