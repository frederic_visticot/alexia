import 'package:flutter/material.dart';
import 'package:monamoureuse/step_port_question_page.dart';

class StepPortIndicationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Text(
                    "Bravo ! Maintenant, il faut que tu te rendes à cet endroit",
                    style: TextStyle(fontSize: 24),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Image.asset("step1.webp")),
                //child: Image.network(
                //   "https://firebasestorage.googleapis.com/v0/b/alexia-2fe40.appspot.com/o/step1.webp?alt=media&token=b9d6f5d5-72e1-4e43-8646-ab559ca7a5cd"))
              ],
            ),
          ),
          ElevatedButton(
            child: Text("Je suis arrivée"),
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => StepPortQuestionPage()));
            },
          )
        ]),
      ),
    );
  }
}
