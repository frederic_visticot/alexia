import 'package:flutter/material.dart';
import 'package:monamoureuse/step_find_gift_page.dart';
import 'package:url_launcher/url_launcher.dart';

class StepPortQuestionPage extends StatefulWidget {
  @override
  _StepPortQuestionPageState createState() => _StepPortQuestionPageState();
}

class _StepPortQuestionPageState extends State<StepPortQuestionPage> {
  int nbAttempts = 0;
  int resultOK = 1742;

  @override
  void initState() {
    super.initState();
    _startPubTimer();
  }

  _startPubTimer() async {
    Future.delayed(Duration(seconds: 1), () {
      _showPub();
    });
  }

  _checkResult(int result) {
    if (result == resultOK) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => StepFindGiftPage()));
    } else {
      nbAttempts++;
      _showError();
    }
  }

  Future<void> _showPub() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Jeux concours'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    "C'est l'heure de notre jeux concours pour gagner non pas 250000 euros mais 1 repas pour 2 dans votre Peponne préféré.\n\r Il vous suffit d'envoyer par SMS la réponse à la question suivante.\n\n Combien de temps faut il attendre pour manger des lasagnes ?\n\r\n\r Envoyez 1 au 0674639362 si c'est 5 minutes\n\r Envoyez 2 au 0674639362 si c'est 20 minutes"),
                TextButton(
                  child: Text('Envoyer la réponse par SMS'),
                  onPressed: () {
                    launch("sms://0674639362");
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Fermer la fenêtre'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showError() async {
    var lost = false;
    var message = "Concentre toi autrement c'est perdu ...";
    var nbAttemptsMessage = "Plus que ${2 - nbAttempts} essai !";
    if (2 - nbAttempts == 0) {
      nbAttemptsMessage = "Plus d'essai !";
    }
    if (nbAttempts == 2) {
      nbAttempts = 0;
      message = "Tu as de la chance, tu as vies illimitées";
      lost = true;
    }
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Attention mauvaise réponse'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(nbAttemptsMessage),
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK, je vais me concentrer'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
            child: Column(
              children: [
                Center(
                  child: Text(
                    "Quel nombre est écrit en haut à droite du panneau ?",
                    style: TextStyle(fontSize: 24),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Image.asset("step2.webp")),
                SizedBox(
                  height: 40,
                ),
                Column(
                  children: [
                    ButtonTheme(
                      minWidth: 200,
                      child: ElevatedButton(
                        child: Text("1733"),
                        onPressed: () {
                          _checkResult(1733);
                        },
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      child: Text("1742"),
                      onPressed: () {
                        _checkResult(1742);
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      child: Text("1689"),
                      onPressed: () {
                        _checkResult(1689);
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      child: Text("1975"),
                      onPressed: () {
                        _checkResult(1975);
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
