import 'package:flutter/material.dart';
import 'package:monamoureuse/step_call_me_page.dart';
import 'package:monamoureuse/step_conclusion_bof_page.dart';

class StepConclusionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Dis moi si tu as trouvé ca Fun ?",
                  style: TextStyle(fontSize: 24),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 40,
                ),
                Column(
                  children: [
                    ElevatedButton(
                      child: Text("Oui"),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => Confetti()));
                      },
                    ),
                    SizedBox(height: 40),
                    ElevatedButton(
                      child: Text("Bof"),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => StepConclusionBof()));
                      },
                    )
                  ],
                )
              ],
            )),
          ),
        ]),
      ),
    );
  }
}
