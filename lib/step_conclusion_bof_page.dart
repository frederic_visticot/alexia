import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class StepConclusionBof extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 50),
        child: Column(children: [
          Flexible(
            child: Center(
              child: Text(
                "Désolé si tu n 'as pas aimé, Téléphone à la production pour te plaindre",
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          ElevatedButton(
            child: Text("Contacter la production"),
            onPressed: () {
              launch("tel://0141861400");
            },
          )
        ]),
      ),
    );
  }
}
