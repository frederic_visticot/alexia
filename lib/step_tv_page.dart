import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:monamoureuse/step_count_love_page.dart';

class StepTvPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Flexible(
          child: Center(
            child: Text(
              "Cherche une boite près de la TV.\n\r Clique sur le bouton lorsque tu l'as ouverte",
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 50),
          child: ElevatedButton(
            child: Text("J'ai ouvert la boite"),
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => StepCountLovePage()));
            },
          ),
        )
      ]),
    );
  }
}
